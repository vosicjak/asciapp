package File

trait File {
  val Opened: Boolean
  val Path: String
  def Close()
  def Open()
}
