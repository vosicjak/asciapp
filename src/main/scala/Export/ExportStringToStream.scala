package Export

import java.io.OutputStream

class ExportStringToStream(outputStream: OutputStream) extends ExportString {
  override def Export(input: String): Unit = exportToStream(input)
  private var closed = false

  protected def exportToStream(text: String): Unit = {

    if (closed)
      throw new Exception("The stream is already closed")

    outputStream.write(text.getBytes("UTF-8"))
    outputStream.flush()
  }

  def close(): Unit = {
    if (closed)
      return

    outputStream.close()
    closed = true
  }

}
