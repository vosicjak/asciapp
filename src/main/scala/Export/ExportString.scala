package Export

trait ExportString extends Export[String] {
  override def Export(input: String): Unit
}
