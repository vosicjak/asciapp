package Export

trait Export[T] {
  /**
   * Exports something somewhere
   * @param item The item to export
   */
  def Export(input: T): Unit
}
