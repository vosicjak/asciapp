package ASCIConvertorApp.Model.Table.TablesInt.Linear

import ASCIConvertorApp.Model.Table.TablesInt.TableLinear

class TableCustom(symbols: String) extends TableLinear {
  override val Symbols: Array[Char] = symbols.toCharArray
}
