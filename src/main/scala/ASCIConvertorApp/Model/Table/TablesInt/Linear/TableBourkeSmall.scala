package ASCIConvertorApp.Model.Table.TablesInt.Linear

import ASCIConvertorApp.Model.Table.TablesInt.TableLinear

class TableBourkeSmall extends TableLinear {
  override val Symbols: Array[Char] = " .:-=+*#%@".toCharArray
}
