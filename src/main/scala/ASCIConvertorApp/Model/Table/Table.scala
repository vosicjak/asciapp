package ASCIConvertorApp.Model.Table

trait Table[T] {
  val Symbols: Array[Char]
  def Translate(Img: Array[T]): String
}
