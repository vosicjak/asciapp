package ASCIConvertorApp.Model.Table

trait TableInt extends Table[Int]{
  override def Translate(Img: Array[Int]): String
}
