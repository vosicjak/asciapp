package ASCIConvertorApp.Model.Parser

import ASCIConvertorApp.View.ConsoleMsg.Errors

trait Parser[F,T] extends Errors[T]{
  def Parse(from: F): Option[T]
  override def ReturnNone(name: String): Option[Nothing] = Errors.ParserError(name)
}
