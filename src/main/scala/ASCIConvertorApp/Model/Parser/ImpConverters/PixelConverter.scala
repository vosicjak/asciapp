package ASCIConvertorApp.Model.Parser.ImpConverters

import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource.{PixelRGB, PixelRandomGrey}

trait PixelConverter[T <: PixelSource] {
  def Int(from: T): Int
}
object PixelConverter {
  //def create[T <: PixelSource : PixelConverter]: PixelConverter[T] = implicitly[PixelConverter[T]]
  implicit object RGBPixelConverter extends PixelConverter[PixelRGB] {
    override def Int(from: PixelRGB): Int =
      ((0.3 * from.Red) + (0.59 * from.Green) + (0.11 * from.Blue)).toInt
  }

  implicit object GreyPixelConverter extends PixelConverter[PixelRandomGrey] {
    override def Int(from: PixelRandomGrey): Int = from.Grey
  }
}
