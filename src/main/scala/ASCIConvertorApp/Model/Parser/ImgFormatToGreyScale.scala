package ASCIConvertorApp.Model.Parser

import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.{PixelGreyScale, PixelSource}

class ImgFormatToGreyScale[T <: PixelSource] extends Parser[ImageFormat[T], ImageFormat[PixelGreyScale]] {
  val pixelParser: PixelToGreyScale[T] = PixelToGreyScale.apply[T]

  override def Parse(from: ImageFormat[T]): Option[ImageFormat[PixelGreyScale]] = {
    val newData: Array[Array[PixelGreyScale]] = Array.ofDim[PixelGreyScale](from.Height, from.Width)
    for(y <- newData.indices) {
      for(x <- newData(y).indices) {
        pixelParser.Parse(from.Data(y)(x)) match{
          case Some(pixel) => newData(y)(x) = pixel
          case None => ReturnNone("ImgFormatToGreyScale")
        }
      }
    }
    CheckOutput(ImageFormat.Apply(newData), "ImgFormatToGreyScale")
  }
}
