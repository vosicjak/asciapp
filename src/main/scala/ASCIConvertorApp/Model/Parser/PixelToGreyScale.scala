package ASCIConvertorApp.Model.Parser

import ASCIConvertorApp.Model.DatStruct.Pixel.PixelGreyScale
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource
import ASCIConvertorApp.Model.Parser.ImpConverters.PixelConverter


class PixelToGreyScale[T <: PixelSource](implicit val converter: PixelConverter[T]) extends Parser[T, PixelGreyScale] {
  override def Parse(from: T): Option[PixelGreyScale] = {
    CheckOutput(PixelGreyScale.Apply(converter.Int(from)), "ParserRGBPixelToGreyPixel")
  }
}

object PixelToGreyScale {
  def apply[T <: PixelSource : PixelConverter]: PixelToGreyScale[T] = new PixelToGreyScale[T]()
}

