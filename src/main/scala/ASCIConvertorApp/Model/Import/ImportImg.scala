package ASCIConvertorApp.Model.Import

import ASCIConvertorApp.Model.DatStruct.ImageFormat

trait ImportImg[T] extends Import[ImageFormat[T]]{
  override def Imp(): Option[ImageFormat[T]]
}
