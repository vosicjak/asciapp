package ASCIConvertorApp.Model.Import

import ASCIConvertorApp.View.ConsoleMsg.Errors

trait Import[T] extends Errors[T]{
  def Imp(): Option[T]
  override def ReturnNone(name: String): Option[Nothing] = Errors.ImportError(name)
}
