package ASCIConvertorApp.Model.Import.ImportImg

import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource.PixelRGB
import ASCIConvertorApp.Model.Import.ImportImg
import File.FileImg

trait RGBFile extends ImportImg[PixelRGB]{
  val ImgFile: FileImg
}
