package ASCIConvertorApp.Model.Import.ImportImg

import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource.{PixelRGB, PixelRandomGrey}
import ASCIConvertorApp.Model.Import.ImportImg

import scala.util.Random

class GreyRandom extends ImportImg[PixelRandomGrey]{
  val random = new Random
  def RndIntInRange(from: Int, to: Int): Int = random.nextInt(to - from + 1) + from

  override def Imp(): Option[ImageFormat[PixelRandomGrey]] = {
    val width = RndIntInRange(1,10) * 10
    val height = RndIntInRange(1,10) * 10
    val data = Array.ofDim[PixelRandomGrey](height, width)
    for(y <- 0 until height)
      for(x <- 0 until width)
        data(y)(x) = PixelRandomGrey(RndIntInRange(0,255))
    CheckOutput(ImageFormat.Apply(data),"GreyRandomImg")
  }
}
