package ASCIConvertorApp.Model.Import.ImportImg

import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource.PixelRGB
import File.FileImg

class RGBFileJpg(file: FileImg) extends RGBFile {
  override val ImgFile: FileImg = file
  override def Imp(): Option[ImageFormat[PixelRGB]] = {
    ???
  }
}
