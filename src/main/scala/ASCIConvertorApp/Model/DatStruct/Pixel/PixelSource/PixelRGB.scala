package ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource

import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource
import ASCIConvertorApp.View.ConsoleMsg.Errors

case class PixelRGB private(Red: Int, Green: Int, Blue: Int) extends PixelSource{
  //def GrayScaleValue(): Int = ((0.3 * Red) + (0.59 * Green) + (0.11 * Blue)).toInt
}

object PixelRGB{
  def Apply(red: Int, green: Int, blue: Int): Option[PixelRGB] = {
    if(ColorScale(red) && ColorScale(green) && ColorScale(blue))
      Some(PixelRGB(red, green, blue))
    else
      Errors.FactoryError("PixelRGB")
  }
  private def ColorScale(color: Int): Boolean = color >= 0 && color < 256
}
