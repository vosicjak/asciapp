package ASCIConvertorApp.Model.DatStruct.Pixel

case class PixelGreyScale private(GreyScale: Int) extends Pixel{
}

object PixelGreyScale{
  def Apply(greyScale: Int): Option[PixelGreyScale] = Some(PixelGreyScale(GreyScale(greyScale)))
  private def GreyScale(color: Int): Int = {
    color match{
      case c if c < 0 => 0
      case c if c > 255 => 255
      case c => c
    }
  }
}