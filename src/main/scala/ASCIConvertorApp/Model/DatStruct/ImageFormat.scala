package ASCIConvertorApp.Model.DatStruct

import ASCIConvertorApp.View.ConsoleMsg.Errors

case class ImageFormat[T] private (Data: Array[Array[T]], Height: Int, Width: Int){
  def GetAt(y: Int, x: Int): Option[T] = {
    if (y < 0 || y > Height || x < 0 || x > Width)
      None
    else
      Some(Data(y)(x))
  }
  def GetRow(y: Int): Option[List[T]] = {
    if (y < 0 || y > Height)
      None
    else
      Some(Data(y).toList)
  }
}

object ImageFormat{
  def Apply[T](data: Array[Array[T]]): Option[ImageFormat[T]] = {
    if (data.length == 0 || data(0).length == 0 || data.map(_.length).toSet.size != 1) Errors.FactoryError("ImageFormat")
    else Some(ImageFormat[T](data, data.length, data(0).length))
  }
}
