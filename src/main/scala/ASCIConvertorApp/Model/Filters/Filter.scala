package ASCIConvertorApp.Model.Filters

import ASCIConvertorApp.View.ConsoleMsg.Errors

trait Filter[T] extends Errors[T]{
  def Apply(input: T): Option[T]
  override def ReturnNone(name: String): Option[Nothing] = Errors.FilterError(name)
}
