package ASCIConvertorApp.Model.Filters

import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelGreyScale

trait FilterImgGrey extends FilterImg[PixelGreyScale]{
  override def Apply(input: ImageFormat[PixelGreyScale]): Option[ImageFormat[PixelGreyScale]]
}
