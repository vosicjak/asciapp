package ASCIConvertorApp.Model.Filters.FiltersImgGrey

import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelGreyScale

class FlipRotate90(r: Int) extends Flip{
  private val Rot: Int = r
  override def Apply(input: ImageFormat[PixelGreyScale]): Option[ImageFormat[PixelGreyScale]] = {
     Rot % 360 match{
        case 90 => CheckOutput(ImageFormat.Apply(FlipAxisDiagonal(FlipAxisByY(input.Data))),"FlipRotate90")
        case 270 => CheckOutput(ImageFormat.Apply(FlipAxisByY(FlipAxisDiagonal(input.Data))),"FlipRotate90")
        case 180 => CheckOutput(ImageFormat.Apply(FlipAxisByY(FlipAxisByX(input.Data))),"FlipRotate90")
        case 0 => Some(input)
        case _ => ReturnNone("FlipRotate90")
      }
  }
}
/* 90 -fy: ->v ^-> v->
* AB CA AC
* CD DB BD
* -fy 90: ->v <-v ^<-
* AB BA DB
* CD DC CA
* -fx 90: ->v ->^ v-> (2. opak = --flip x)
* AB CD AC
* CD AB BD
* -fx -fy 90: ->v ->^ <-^ v<- (2. opak + 1.opak)
* AB CD DC BD
* CD AB BA AC
* 90 90 90 90: ->v ^-> <-^ v<- (prohození + 1. po prohození je opak = --rotate 90)
* AB CA DC BD
* CD DB BA AC
* */