package ASCIConvertorApp.Model.Filters.FiltersImgGrey

import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelGreyScale
import ASCIConvertorApp.Model.Filters.FilterImgGrey

class MixedFilter(seq: Seq[FilterImgGrey]) extends FilterImgGrey {
  override def Apply(input: ImageFormat[PixelGreyScale]): Option[ImageFormat[PixelGreyScale]] = {
    var out = input
    for(filter <- seq){
      filter.Apply(out) match{
        case Some(filtered) => out = filtered
        case None => return ReturnNone("MixedFilter")
      }
    }
    Some(out)
  }
}
