package ASCIConvertorApp.Model.Filters.FiltersImgGrey

import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelGreyScale
import ASCIConvertorApp.View.ConsoleMsg.Errors

class FlipX extends Flip{
  override def Apply(input: ImageFormat[PixelGreyScale]): Option[ImageFormat[PixelGreyScale]] =
    CheckOutput(ImageFormat.Apply(FlipAxisByX(input.Data)), "FlipX")
}
