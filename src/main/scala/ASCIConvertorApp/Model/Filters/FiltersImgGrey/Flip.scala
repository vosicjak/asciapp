package ASCIConvertorApp.Model.Filters.FiltersImgGrey

import ASCIConvertorApp.Model.Filters.FilterImgGrey

trait Flip extends FilterImgGrey {
  def FlipAxisDiagonal[T](array: Array[Array[T]]): Array[Array[T]] = array.transpose
  def FlipAxisByY[T](array: Array[Array[T]]): Array[Array[T]] = array.reverse
  def FlipAxisByX[T](array: Array[Array[T]]): Array[Array[T]] = {
    for(arr <- array) arr.reverse
    array
  }
}
