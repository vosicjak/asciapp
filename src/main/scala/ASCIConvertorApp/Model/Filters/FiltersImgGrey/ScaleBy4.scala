package ASCIConvertorApp.Model.Filters.FiltersImgGrey

import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelGreyScale
import ASCIConvertorApp.Model.Filters.FilterImgGrey

class ScaleBy4(nextSize: Double) extends FilterImgGrey {
  val ScaleAxisBy: Double = math.sqrt(nextSize)
  override def Apply(input: ImageFormat[PixelGreyScale]): Option[ImageFormat[PixelGreyScale]] = {
    val NewData: Array[Array[PixelGreyScale]] =
      Array.ofDim[PixelGreyScale](math.ceil(input.Height * ScaleAxisBy).toInt, math.ceil(input.Width * ScaleAxisBy).toInt)
    for (y <- NewData.indices) {
      for (x <- NewData(y).indices) {
        input.GetAt((y / ScaleAxisBy).toInt, (x / ScaleAxisBy).toInt) match{
          case Some(pixel) => NewData(y)(x) = pixel
          case None => return ReturnNone("ScaleBy4")
        }
      }
    }
    CheckOutput(ImageFormat.Apply(NewData),"ScaleBy4")
  }
}
