package ASCIConvertorApp.Model.Filters

import ASCIConvertorApp.Model.DatStruct.ImageFormat

trait FilterImg[T] extends Filter[ImageFormat[T]]{
  override def Apply(input: ImageFormat[T]): Option[ImageFormat[T]]
}
