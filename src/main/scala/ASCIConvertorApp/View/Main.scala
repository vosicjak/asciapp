package ASCIConvertorApp.View

import ASCIConvertorApp.Conntroller.ControllerConsole


object Main extends App{
  val controller = new ControllerConsole()
  val view = new ViewConsole(controller,args)
  view.Run()
}