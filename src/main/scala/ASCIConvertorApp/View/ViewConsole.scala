package ASCIConvertorApp.View

import ASCIConvertorApp.Conntroller.Controller
import ASCIConvertorApp.Model.Import.ImportImg.GreyRandom
import ASCIConvertorApp.Model.Table.TablesInt.Linear.{TableBourkeSmall, TableCustom}
import ASCIConvertorApp.Model.Table.TablesInt.NonLinear.TableTest
import Export.ExportsStringStream.{ExportToConsole, ExportToTxtFile}

class ViewConsole(controller: Controller, args: Array[String]) extends View{
  override def Run(): Unit = {
    var argIterator = args.iterator
    while (argIterator.hasNext)
      InputCommand(argIterator)
    controller.Export()
  }

  def CheckSecondParameter(it: Iterator[String]): String = {
    if (it.hasNext)
      it.next()
    else
      "<-Missing->"
  }

  def PrintMissingPar(e: String): Unit = println(e + ": \"???\" - is missing parameter")

  private def InputCommand(argIterator: Iterator[String]): Unit = {
    argIterator.next() match {
      case "--image-random" => controller.Import(new GreyRandom())
      case "--image" => CheckSecondParameter(argIterator) match {
        case path => controller.Import(???)     //TODO
        case e => println("--image: " + e + " - is wrong parameter")
      }
      case "--output-console" => controller.AddExport(new ExportToConsole())
      case "--output-file" => CheckSecondParameter(argIterator) match {
        case path => controller.AddExport(new ExportToTxtFile(???)) //TODO
        case e => println("--output-file: " + e + " - is wrong parameter")
      }
      case "--table" => CheckSecondParameter(argIterator) match {
        case "bourke" => controller.SelectTable(new TableBourkeSmall())
        case "dark" => controller.SelectTable(new TableTest())
        case e => println("--table: " + e + " - is wrong parameter")
      }
      case "--custom-table" => CheckSecondParameter(argIterator) match {
        case symbols => controller.SelectTable(new TableCustom(symbols))
        case e => println("--custom-table: " + e + " - is wrong parameter")
      }
      case "--rotate" => CheckSecondParameter(argIterator) match {
        case number if number.toIntOption.isDefined => {
          if (number.toInt % 90 == 0) controller.Rotate90(number.toInt)
          else println("--rotate: " + number + " - is not mod 90")
        }
        case e => println("--rotate: " + e + " - is wrong parameter")
      }
      case "--flip" => CheckSecondParameter(argIterator) match {
        case "x" => controller.Flip('x')
        case "y" => controller.Flip('y')
        case e => println("--flip: " + e + " - is wrong parameter")
      }
      case "--scale" => CheckSecondParameter(argIterator) match {
        case number if number.toDoubleOption.isDefined => number.toDouble match {
          case 1.0 => None
          case number if (1 / number) % 4 == 0 => controller.Scale(number)
          case number if number % 4 == 0 => controller.Scale(number)
          case e => println("--scale: " + e + " - this number is not implemented")
        }
        case e => println("--scale: " + e + " - is wrong parameter")
      }
      case "--font-aspect-ratio" => println("--font-aspect-ratio not implemented")
      case e => println("\"" + e + "\" - unknown argument")
    }
  }
}

/*object Commands extends Enumeration {
  type Command = Value

  val image: Commands.Value = Value("--image")
  val imageRandom: Commands.Value = Value("--image-random")
  val table: Commands.Value = Value("--table")
  val tableCustom: Commands.Value = Value("--custom-table")
  val filterRotate: Commands.Value = Value("--rotate")
  //val filterInv: Commands.Value = Value("--invert")
  val filterFlip: Commands.Value = Value("--flip")
  //val filterBright: Commands.Value = Value("--brightness")
  val outputConsole: Commands.Value = Value("--output-console")
  val outputFile: Commands.Value = Value("--output-file")

  def isCommand(cmd: String): Boolean = values.exists(_.toString == cmd)
  def isInput(cmd: String): Boolean = values.exists(_.toString == cmd)
}

object Tables extends Enumeration {

  type Table = Value

  val conversionBourke: Tables.Value = Value("bourke")
  val conversionConstant: Tables.Value = Value("constant")

  def isTable(table: String): Boolean = values.exists(_.toString == table)

}*/
