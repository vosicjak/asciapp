package ASCIConvertorApp.View.ConsoleMsg

import Export.ExportsStringStream.ExportToConsole

object Errors{
  val ExportStringToStream = new ExportToConsole()
  def FilterError(name: String): Option[Nothing] = {
    ExportStringToStream.Export("Filter: " + name + " - failed\n")
    None
  }

  def FactoryError(name: String): Option[Nothing] = {
    ExportStringToStream.Export("Factory: " + name + " - failed\n")
    None
  }

  def ImportError(name: String): Option[Nothing] = {
    ExportStringToStream.Export("Import: " + name + " - failed\n")
    None
  }

  def ParserError(name: String): Option[Nothing] = {
    ExportStringToStream.Export("Parser: " + name + " - failed\n")
    None
  }
}

trait Errors[T]{
  def ReturnNone(name: String): Option[Nothing]
  def CheckOutput(output: Option[T], name: String): Option[T] = {
    output match {
      case None => ReturnNone(name)
      case other => other
    }
  }
}

