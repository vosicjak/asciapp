package ASCIConvertorApp.Conntroller

import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource.PixelRGB
import ASCIConvertorApp.Model.Table.TableInt
import ASCIConvertorApp.Model.Import.ImportImg
import ASCIConvertorApp.View.ConsoleMsg.Errors
import Export.ExportStringToStream

trait Controller{
  def Import[T <: PixelSource](input: ImportImg[T]): Unit
  def Rotate90(degree: Int): Unit
  def Flip(axis: Char): Unit
  def Scale(scale: Double): Unit
  def SelectTable(table: TableInt): Unit
  def AddExport(output: ExportStringToStream): Unit
  def Export(): Unit
}