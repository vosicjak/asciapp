package ASCIConvertorApp.Conntroller

import ASCIConvertorApp.Model.DatStruct.Pixel.{PixelGreyScale, PixelSource}
import ASCIConvertorApp.Model.DatStruct.ImageFormat
import ASCIConvertorApp.Model.DatStruct.Pixel.PixelSource.PixelRGB
import ASCIConvertorApp.Model.Filters.FilterImgGrey
import ASCIConvertorApp.Model.Import.{Import, ImportImg}
import ASCIConvertorApp.Model.Parser.ImgFormatToGreyScale
import ASCIConvertorApp.Model.Table.TableInt
import Export.ExportStringToStream

class ControllerConsole extends Controller {
  private val listFilter: List[FilterImgGrey] = List()
  var imgFormat: Option[ImageFormat[PixelGreyScale]] = None
  val exporters: List[ExportStringToStream] = List()


  override def Import[T <: PixelSource](input: ImportImg[T]): Unit = {
    input.Imp() match {
      case Some(img) => imgFormat = new ImgFormatToGreyScale[T].Parse(img)
      case None =>
    }
  }
  override def Rotate90(degree: Int): Unit = ???

  override def Flip(axis: Char): Unit = ???

  override def Scale(scale: Double): Unit = ???

  override def SelectTable(table: TableInt): Unit = ???

  override def AddExport(output: ExportStringToStream): Unit = ???

  override def Export(): Unit = ???
}
